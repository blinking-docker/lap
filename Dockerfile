FROM php:7-apache

# install the PHP extensions we need
RUN apt-get update && apt-get install -y wget libpng12-dev libjpeg-dev libpq-dev \
        && rm -rf /var/lib/apt/lists/* \
        && docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
        && docker-php-ext-install gd mbstring pdo pdo_mysql pdo_pgsql zip

# install composer
RUN curl -sS https://getcomposer.org/installer | php  && mv composer.phar /usr/local/bin/composer

# install git and add gitlab to know hosts
RUN apt-get update && apt-get install -y ca-certificates git-core ssh && rm -rf /var/lib/apt/lists/* \ 
    && mkdir /root/.ssh && ssh-keyscan gitlab.com > /root/.ssh/known_hosts

#Enable mod_rewrite
RUN a2enmod rewrite 
